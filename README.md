React app boilerplate
=====================

A simpler and less opinionated React app boilerplate, with [LESS] support for CSS pre-processing.

* No forced directories structure for app sources
* [LESS] support for CSS pre-processing, with [Autoprefixer] configured using [Browserslist]
* [LiveReload] built-in server compatible with browser extensions
* [Webpack] build system, with desktop notifications for errors and [bundle size analysis][webpack-bundle-analyzer]
* [Babel] configured with [`preset-env`][preset-env], automatically importing [`corejs@3`][core-js] polyfills
* No bundling of assets: HTML, images and styles served as normal resources
* [Lodash] and [Classnames] already installed, with [automatic][babel-plugin-lodash] cherry-picking of Lodash methods
* [Jest] and [Enzyme] ready for component testing
* [ESLint] with sane presets

Usage
-----

Clone and "clear" this repository:

	cd new-project
	git clone "https://bitbucket.org/lorenzostanco/react-app-boilerplate/" .
	rm -rf README.md .git
	vim package.json # Edit app metadata

Install dependencies, test and build:

	npm install
	npm test # Test with Jest
	npm test -- --watch # Jest in watch mode
	npm run live # Development build
	npm run build # Production build
	npm run build-analyze # Build and analyze bundle size

[LESS]: http://lesscss.org/
[LiveReload]: https://github.com/napcs/node-livereload
[Webpack]: https://webpack.js.org/
[Autoprefixer]: https://github.com/postcss/autoprefixer
[Browserslist]: https://github.com/ai/browserslist
[Babel]: https://babeljs.io/
[preset-env]: https://babeljs.io/docs/en/babel-preset-env
[core-js]: https://github.com/zloirock/core-js
[Lodash]: https://lodash.com/docs/
[Classnames]: https://github.com/JedWatson/classnames
[ESLint]: https://eslint.org/
[babel-plugin-lodash]: https://github.com/lodash/babel-plugin-lodash
[Jest]: http://facebook.github.io/jest/
[Enzyme]: http://airbnb.io/enzyme/
[webpack-bundle-analyzer]: https://github.com/webpack-contrib/webpack-bundle-analyzer

Tips
----

### Polyfills for modules

Some modules (like SuperAgent, axios or Luxon) [may require][superagent on IE] Babel polyfills to work in all browsers. In the Webpack configuration, Babel is excluded from `node_modules`, but it's possible to enable it for specific modules.

	// Grab Babel configuration from package.json
	const babelConfig = require('./package.json').babel;
	
	// Pass it to the babel loader, and remove some modules from the exclusions
	{
		test: /\.jsx?$/, 
		exclude: [/node_modules\/(?!axios|luxon\/)/],
		loader: 'babel-loader',
		options: babelConfig
	}
	
[superagent on IE]: https://github.com/visionmedia/superagent/pull/1465#issuecomment-481123734

### Source type for non-ES modules

Babel's [`sourceType`][sourceType] option can help in situations where Babel inserts `import/export` statement in CommonJS modules, making the code throw *"error: exports is not defined"*  ([1][sourceType issue 1], [2][sourceType issue 2]).

	"babel": {
		"sourceType": "unambiguous"
	}

[sourceType]: https://babeljs.io/docs/en/options#sourcetype
[sourceType issue 1]: https://github.com/babel/babel/issues/8900#issuecomment-431240426
[sourceType issue 2]: https://github.com/babel/babel/issues/9187#issuecomment-447704595
