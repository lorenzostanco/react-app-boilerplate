const path = require('path');
const md5File = require('md5-file');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const CleanWebpackPlugin = require('clean-webpack-plugin').CleanWebpackPlugin;
const LiveReloadPlugin = require('webpack-livereload-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (env, options) => {
	
	const isProduction = options && options.mode == 'production';
	const writeSourceMaps = !isProduction || process.argv.indexOf("--source-maps") > -1;
	const analyze = process.argv.indexOf("--analyze") > -1;
	const outputPath = path.resolve(__dirname, 'build');
	
	const cssLoaders = [
		MiniCssExtractPlugin.loader,
		{
			loader: 'css-loader',
			options: { sourceMap: writeSourceMaps, url: false }
		}, {
			loader: 'postcss-loader',
			options: {
				sourceMap: writeSourceMaps,
				postcssOptions: {
					plugins: [
						autoprefixer,
						isProduction ? cssnano : null
					].filter(p => p != null)
				}
			}
		}
	];
	
	return {
		entry: {
			main: './index.jsx'
		},
		devtool: writeSourceMaps ? "source-map" : false,
		plugins: [
			new CleanWebpackPlugin(),
			fixLiveCssReloading(new LiveReloadPlugin(), outputPath),
			new MiniCssExtractPlugin({ filename: '[name].css' }),
			new WebpackBuildNotifierPlugin({ suppressSuccess: true }),
			analyze ? new BundleAnalyzerPlugin({ analyzerMode: 'static' }) : null
		].filter(p => p != null),
		module: {
			rules: [
				{ 
					test: /\.jsx?$/, 
					resolve: { extensions: [".js", ".jsx"] },
					exclude: [/node_modules/],
					loader: 'babel-loader'
				}, {
					test: /\.less$/,
					use: [ ...cssLoaders, {
						loader: 'less-loader',
						options: { sourceMap: writeSourceMaps }
					}]
				}, {
					test: /\.css$/,
					use: [ ...cssLoaders ]
				}
			]
		},
		output: {
			path: outputPath,
			filename: '[name].js'
		},
		performance: {
			hints: false
		}
	};
	
};

// Avoid full-page refresh when a CSS file changes
// https://github.com/statianzo/webpack-livereload-plugin/issues/28#issuecomment-827025000
const fixLiveCssReloading = function(pluginInstance, outputPath) {
	pluginInstance._afterEmit = (compilation) => {
		const self = pluginInstance;
		const fileHashes = {};
		self.fileHashes = self.fileHashes || {};
		compilation.getAssets().forEach(asset => {
			fileHashes[asset.name] = md5File.sync(`${outputPath}/${asset.name}`);
		});
		const include = Object.keys(fileHashes).filter(file => {
			if (self.options.ignore && file.match(self.options.ignore)) {
				return false;
			}
			return !(file in self.fileHashes) || self.fileHashes[file] !== fileHashes[file];
		});
		if (self._isRunning() && include.length) {
			self.fileHashes = fileHashes;
			self.logger.info('Reloading ' + include.join(', '));
			setTimeout(() => {
				self.server.notifyClients(include);
			}, self.options.delay);
		}
	}
	return pluginInstance;
};
