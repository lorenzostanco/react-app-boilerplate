import React from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import _ from 'lodash';

import './style.less';

class App extends React.Component {

	render() {
		return (
			<div className="App">
				∅ Empty React app...
			</div>
		);
	}

}

ReactDOM.render(<App />, document.getElementById('app'));